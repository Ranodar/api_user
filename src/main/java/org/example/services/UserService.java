package org.example.services;


import org.example.dtos.response.UserDto;

import java.util.List;


public interface UserService {

    public UserDto createUser(UserDto userDto) throws Exception;
    public UserDto readUser(long id);
    public List<UserDto> readUsers();
    public long getIdUserByEmail(String email) throws Exception;

    public long getIdUserByEmailAndPassword(String email, String password) throws Exception;



}
