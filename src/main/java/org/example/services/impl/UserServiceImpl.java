package org.example.services.impl;

import org.example.dtos.response.UserDto;
import org.example.entity.User;
import org.example.repository.UserRepository;
import org.example.services.UserService;
import org.example.tools.DtoTools;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository _userRepository;

    @Autowired
    private DtoTools _dtoTools;


    @Override
    public UserDto createUser(UserDto userDto) throws Exception {
        User user = _dtoTools.convertToEntity(new User(),userDto);

        if (_userRepository.findUserByEmail(user.getEmail()) != null) {
            throw new Exception("Email déjà utilisé");
        }else {
            User user1 = _userRepository.save(user);
            return _dtoTools.convertToDto(user1,new UserDto());
        }
    }

    @Override
    public UserDto readUser(long id) {
        return _dtoTools.convertToDto(_userRepository.findById(id).get(),new UserDto());
    }

    @Override
    public List<UserDto> readUsers() {
        List<UserDto> userDtos = new ArrayList<>();
        List<User> users = (List<User>) _userRepository.findAll();
        for (User user:users){
            userDtos.add(_dtoTools.convertToDto(user, new UserDto()));
        }
        return userDtos;
    }

    @Override
    public long getIdUserByEmailAndPassword(String email, String password) throws Exception {
        try {
            User user = _userRepository.findUserByEmail(email);
            System.out.println(user);
            if (password == user.getPassword()) {
                return user.getId();
            }else {
                throw new Exception("password incorrect");
            }
        }catch (Exception e){
            throw new Exception("utilisateur introuvable");
        }
    }

    public long getIdUserByEmail(String email) throws Exception {
        try {
            User user = _userRepository.findUserByEmail(email);
            return user.getId();
        }catch (Exception e){
            throw new Exception("utilisateur introuvable");
        }
    }
}
