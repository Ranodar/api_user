package org.example.controllers;

import org.example.dtos.response.UserDto;
import org.example.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = {"http://localhost:8080"}, methods = {RequestMethod.GET, RequestMethod.POST})
@RestController
@RequestMapping("/api/user")
public class UserController {

    @Autowired
    private UserService _userService;

    @GetMapping("")
    public ResponseEntity<List<UserDto>> get() {
        return ResponseEntity.ok(_userService.readUsers());
    }

    @GetMapping("/{id}")
    public ResponseEntity getUser(@PathVariable long id){

        try {
            return  ResponseEntity.ok(_userService.readUser(id));
        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("not_found");
        }

    }

    @PostMapping("")
    public ResponseEntity addUser(@RequestBody UserDto userDto) throws Exception {
        UserDto userDtoSave = _userService.createUser(userDto);
        try {
            return new ResponseEntity<>(userDtoSave,HttpStatus.CREATED);
        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body("not_acceptable");
        }
    }

    @GetMapping("/login")
    public ResponseEntity getId(@RequestBody UserDto userDto) throws Exception {
        return ResponseEntity.ok(_userService.getIdUserByEmailAndPassword(userDto.getEmail(), userDto.getPassword()));
    }
//    @RequestParam String email,@RequestParam String password
}
