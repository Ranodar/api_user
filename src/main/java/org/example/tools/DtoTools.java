package org.example.tools;

import org.example.dtos.response.UserDto;
import org.example.entity.User;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class DtoTools {

    public UserDto convertToDto(User user, UserDto userDto){
        return new ModelMapper().map(user, userDto.getClass());
    }

    public User convertToEntity(User user, UserDto userDto){
        return new ModelMapper().map(userDto, user.getClass());
    }
}
